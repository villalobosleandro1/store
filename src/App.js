import React from 'react';
import { ThemeProvider, CSSReset, theme } from "@chakra-ui/core";

import { Layout } from './Layout'
import { Home } from './Components/Home'

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Layout>
        <Home/>
      </Layout>
    </ThemeProvider>
  );
}

export default App;
