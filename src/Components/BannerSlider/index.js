import React from 'react'
import { Grid } from '@chakra-ui/core'
import Carousel  from 'react-bootstrap/Carousel'

export const BannerSlider = () => {
  return (
    <Grid
      borderRadius={10} 
      h='450px'
      templateColumns="1fr"
      // marginBottom={10}
    >

      <Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://placehold.it/350x150?text=Slide1"
            alt="First slideeeeeeeeeeeeeeeeeeeeeeeee"
          />
        </Carousel.Item>

        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://placehold.it/350x150?text=Slide2"
            alt="First slideeeeeeeeeeeeeeeeeeeeeeeee"
          />
        </Carousel.Item>
      </Carousel>
      
    </Grid>
  )
}
