import React from 'react'
import { Grid, Box } from '@chakra-ui/core'

export const Featured = () => {
  return (
    <Grid 
      templateColumns='repeat(3, 400px)' 
      justifyContent="center"
      gap='300px' 
      p='20px'
      // height="200px"
      // marginTop="400px"
    >
        <Box h='400px' bg='red.600' />
        <Box h='400px' bg='red.400' />
        <Box h='400px' bg='red.200' />
      </Grid>
  )
}
