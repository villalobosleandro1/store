import React from 'react'
import { Box, Grid } from '@chakra-ui/core'
import { BannerSlider } from './../BannerSlider'
import { Featured } from './../Featured'

export const Home = () => {
  
  return (
    <Grid>
      <BannerSlider/>
      <Featured/>
    </Grid>
  )
}
