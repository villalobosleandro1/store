import React from 'react'
import { Grid } from "@chakra-ui/core"
import { Navbar } from './../Components/Navbar'

export const Layout = ({children}) => {
  return (
    <Grid>
      <Navbar/>
      <Grid templateColumns='80%' justifyContent='center' >
        { children }
      </Grid>
    </Grid>
  )
}
